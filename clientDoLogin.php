<?php

include_once 'Gioco.php';
include_once 'Configurazione.php';

session_start();

if (isset(($_SESSION['codicePartita'])) && (isset($_SESSION['mioColore']))) {
    header("Location:clientGioco.php");
}

$nome = $_POST['nome'];

if ($nome == "") {
    echo "Errore: inserire il nome";
    exit;
}

$gioco = new Gioco();

if ($gioco->trovaPartitaDisponibile()) {
    // esiste una partita che può essere utilizzata
}
else {
    if (!($gioco->nuovaPartita())) {
        echo "Errore: nessuna partita e impossibile creare una nuova";
        exit;
    }
}

$ret = $gioco->aggiungiGiocatore($nome, $_SERVER['REMOTE_ADDR']);

if (!$ret) {
    echo "Impossibile aggiungere il giocatore!";
    exit;
}
$posizione = strtok($ret, "@");
$colore = strtok("@");

$_SESSION['codicePartita'] = $gioco->getCodicePartita();
$_SESSION['mioColore'] = $colore;

$messaggio = "Codice Partita: ".$gioco->getCodicePartita()." Posizione: ".$posizione." Colore: ".$colore;

//echo $messaggio;
scriviLog("ClientDoLogin", $messaggio, "DEBUG");

Header("Location:clientGioco.php");