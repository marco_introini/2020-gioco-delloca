<?php

/**
 * Description of Azione
 *
 * @author Marco Introini
 */

include_once 'Configurazione.php';

class Azione {
    
    private $azioni;
    private $comandi;
    
    function __construct() {
        $this->azioni = [];
        
        $this->caricaComandi();  
        $this->caricaAzioni();
    }

    private function caricaAzioni() {
        global $fileAzioni;
        
        $file = fopen($fileAzioni,"r");

        while(! feof($file))
        {
            $riga = trim(fgets($file));
            array_push($this->azioni,$riga );
        }

        fclose($file);
        
        //scriviLog(__METHOD__, "Azioni Caricare da file\n". print_r($this->azioni,TRUE), "DEBUG");          
    }
    
    private function caricaComandi() {
        
        $this->comandi = [
            "+1" => "Avanti di una casella",
            "-1" => "Indietro di una casella",
            "+2" => "Avanti di due caselle",
            "-2" => "Indietro di due caselle",
            "+3" => "Avanti di tre caselle",
            "-3" => "Indietro di tre caselle",
            "f1" => "Fermo un turno",
            "ta" => "Tira Ancora",
            "xx" => "Torna all'inizio",
            "na" => "Nessuno"
        ];
        
        //scriviLog(__METHOD__, "Comandi\n". print_r($this->comandi,TRUE), "DEBUG");
    }
    
    
    public function getAzione() {
        $numElementi = count($this->azioni);
        
        // trovo un'azione a caso
        $numero = rand(1,$numElementi) - 1;
        
        $azione = strtok($this->azioni[$numero], "@");
        $comando = strtok("@");
        $descrizioneComando = $this->comandi[$comando];
        
        $ret = [
            "azione" => $azione,
            "codiceComando" => $comando,
            "descrizioneComando" => $descrizioneComando
        ];
        
        return $ret;
    }
    
    
    
}
