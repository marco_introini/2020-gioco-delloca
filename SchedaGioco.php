<?php

include_once 'Configurazione.php';

/**
 * Classe della scheda del gioco
 * Definisce cosa fare quando si arriva su una casella
 *
 * @author Marco Introini
 */
class SchedaGioco {
    
    private $comandi;
    private $azioni;
    private $tavoliereAzioni;
    private $azioneNulla = "Nessuna azione in questa casella@na";
    
    function __construct() {
        global $numeroCaselle;
        
        $this->azioni = [];
        
        $this->caricaAzioni();
        $this->caricaComandi();
        $this->tavoliereAzioni = [];
        for ($i=1; $i<=$numeroCaselle; $i++){
            $this->tavoliereAzioni[$i] = "";
        }
        $this->generaTavoliere();
    }

    private function caricaAzioni() {
        global $fileAzioni;
        
        $file = fopen($fileAzioni,"r");

        while(! feof($file))
        {
            $riga = trim(fgets($file));
            array_push($this->azioni,$riga );
        }

        fclose($file);
        
        scriviLog(__METHOD__, "Azioni Caricare da file\n". print_r($this->azioni,TRUE), "DEBUG");          
    }
    
    private function caricaComandi() {
        
        $this->comandi = [
            "+1" => "Avanti di una casella",
            "-1" => "Indietro di una casella",
            "+2" => "Avanti di due caselle",
            "-2" => "Indietro di due caselle",
            "+3" => "Avanti di tre caselle",
            "-3" => "Indietro di tre caselle",
            "f1" => "Fermo un turno",
            "ta" => "Tira Ancora",
            "xx" => "Torna all'inizio",
            "na" => "Nessuno"
        ];
        
        scriviLog(__METHOD__, "Comandi\n". print_r($this->comandi,TRUE), "DEBUG");
        
    }
    
    private function generaTavoliere() {
        // ciclo all'interno del vettore delle azioni (per essere sicuro di non
        // metterli due volte) e li cairco in posti random
        
        foreach ($this->azioni as $azione) {
            $posizione = $this->trovaPosizione();
            $this->tavoliereAzioni[$posizione] = $azione;
        }
        
        // dopo aver caricato tutte le azioni presenti nel file trovo le caselle
        // senza azioni e inserisco li l'azione nulla
        $posizione = 1;
        foreach ($this->tavoliereAzioni as $casella){
            if ($casella=="") {
                $this->tavoliereAzioni[$posizione] = $this->azioneNulla;
            }
            $posizione++;
        }
   
        scriviLog(__METHOD__, "Tavoliere Azioni Generato\n". print_r($this->tavoliereAzioni,TRUE), "DEBUG");
        
    }
    
    private function trovaPosizione() {
        global $numeroCaselle;
        $postoOk = null;
        while ($postoOk==null) {
            $rand = rand(1,$numeroCaselle-1); // uso -1 in quanto l'ultima casella la lascio libera
            if ($this->tavoliereAzioni[$rand]=="") {
                $postoOk = $rand;
            }
        }
        return $postoOk;
    }
    
    public function stampaTavoliereGioco() {
        // per debug: stampa la schacchiera
        global $numeroCaselle;
        
        echo 'CASELLE\n';
        for ($i=1;$i<=$numeroCaselle;$i++) {
            echo "POSIZIONE $i: AZIONE:". $this->azioneCasella($i)." COMANDO: "
                    .$this->comandoCasella($i)."DESCRIZIONE COMANDO:"
                    .$this->descrizioneComandoCasella($i)."\n";
        }
    }
    
    public function infoCasella($numeroCasella) {
        global $numeroCaselle;
        
        if ($numeroCasella>$numeroCaselle) {
            return "0@Numero casella non valido";
        }
        
        if ($this->tavoliereAzioni[$numeroCasella]) {
            $comando  = strtok($this->tavoliereAzioni[$numeroCasella],"@");
            $azione = strtok("@");
            return "1@".$comando."@".$azione;
        }
    }
    
    public function azioneCasella($numeroCasella) {
        $ret = strtok($this->infoCasella($numeroCasella), "@");

        if ($ret==1) {
            return strtok("@");
        }        
    }
    
    public function comandoCasella($numeroCasella) {
        $ret = strtok($this->infoCasella($numeroCasella), "@");
        
        if ($ret==1) {
            strtok("@");
            return strtok("\n");
        }
    }
    
    public function descrizioneComandoCasella($numeroCasella) {
        $ret = strtok($this->infoCasella($numeroCasella), "@");
        $azione = "na";
        
        if ($ret==1) {
            strtok("@");
            $azione = strtok("@");
        }        
        
        return $this->comandi[$azione];
    }
    
    
}