<?php

include_once 'Configurazione.php';
include_once 'Partita.php';

session_start();

if (!isset(($_SESSION['codicePartita']))||(!isset($_SESSION['mioColore']))) {
    header("Location:clientLogin.php");
}

header("Location:clientGioco.php");

?>