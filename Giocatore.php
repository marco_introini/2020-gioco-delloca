<?php

/**
 * Classe che definisce il giocatore
 *
 * @author Marco Introini
 */

include_once 'Configurazione.php';

class Giocatore {
    private $nome;
    private $colore;
    private $ip;
    private $casella;
    private $saltaTurno;
    
    function __construct($nome,$colore,$ip) {
        $this->nome = $nome;
        $this->colore = $colore;
        $this->ip = $ip;
        $this->casella = 0;
        $this->saltaTurno = false;
    }

    public function getPosizione() {
        return $this->casella;
    }
    
    public function setPosizione($nuovaPosizione) {
        global $numeroCaselle;
        
        if ($nuovaPosizione > $numeroCaselle) {
            $diff = $nuovaPosizione - $numeroCaselle;
            $nuovaPosizione = $numeroCaselle - $diff;
        }
        
        $this->casella = $nuovaPosizione;
    }
    
    public function avanza($caselle) {
        
        $nuovaPosizione = $this->getPosizione() + $caselle;
          
        $this->setPosizione($nuovaPosizione);
        
        return $this->getPosizione();
    }
    
    public function getSaltaTurno() {
        return $this->saltaTurno;
    }
    
    public function setSaltaTurno($salta) {
        $this->saltaTurno = $salta;
    }
    
    public function getColore() {
        return $this->colore;
    }
    
    public function getNome() {
        return $this->nome;
    }
    
    public function controllaIp($ip) {
        if ($ip==$this->ip) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function verificaNome($nome) {
        if ($nome == $this->nome) {
            return true;
        }
        else {
            return false;
        }
    }
    
}
