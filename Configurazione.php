<?php

$numeroCaselle = 40;
$numeroMinimoGiocatori = 2;
$fileAzioni = "azioni.txt";
$tempoTimeoutPartita = 60*60; // tempo partita massimo in secondi: 1 ora
$tempoTimeoutTurno = 5*60; // tempo turno massimo in secondi

// DATABASE
$db_host = "localhost";
$db_name = "zxppcsjs_viola";
$db_user = "zxppcsjs_viola";
$db_password = "Viola,20100";

// DEBUG
$debugMode = true;
$logDir = "log";

// Funzioni utili

function scriviLog($metodo, $messaggio, $logLevel="INFO") {
    global $debugMode;
    global $logDir;
    
    if (!$debugMode && $logLevel=="DEBUG") {
        return;
    }
    
    if (!file_exists($logDir)) 
    {
        // create directory/folder uploads.
        mkdir($logDir, 0777, true);
    }
    $log_file_data = $logDir.'/log_' . date('d-M-Y') . '.log';   
    $logMess = "[$logLevel] ".date("Y-m-d, G:i:s")." - Metodo: $metodo - Messaggio: $messaggio \n"; 
    file_put_contents($log_file_data, $logMess, FILE_APPEND);
}

function millisecondi() {
    $mt = explode(' ', microtime());
    return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
}
