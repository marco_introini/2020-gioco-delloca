<?php

include_once 'Configurazione.php';
include_once 'Gioco.php';

// tutte le chiamate devono arrivare con codicePartita e colore

$codicePartita = $_REQUEST['codicePartita'];
$mioColore = $_REQUEST['mioColore'];
$funzione = $_REQUEST['funzione'];

if ($codicePartita==null || $mioColore==null ) {
    scriviLog("masterServer", "Arrivata chiamata errata: codicePartita $codicePartita colore $mioColore", "DEBUG");
    echo "Errore nella chiamata";
    exit;
}

$gioco = new Gioco();
if (!$gioco->caricaPartita($codicePartita)) {
    scriviLog("masterServer", "Errore nel caricamento della partita $codicePartita", "INFO");
    echo "Errore caricamento partita";
    exit;
}

if ($funzione=="getStato") {
    echo $gioco->getStatoPartita();
    echo "@";
    echo $gioco->getStatoPartitaLeggibile();
    
    if ($gioco->getStatoPartita()==1) {
        // se la partita è in corso restituisco le posizioni dei giocatori
        // numero giocatori@posizione1@posizione2 ecc...
        echo "@";
        echo $gioco->getPosizioniGiocatori();
    }
    
    if ($gioco->getStatoPartita()==0) {
        // se la partita non è ancora iniziata torno le info sui giocatori registrati
        // numero giocatori attuali@nome1@nome2 ecc...
        echo "@";
        echo $gioco->getNomiGiocatori();
    }
    
    if ($debugMode) {
        echo "@";
        echo "\n\nDEBUG MODE\n\n";
        echo $gioco->getStringaDebug();
    }
    exit();
}

if ($funzione=="iniziaPartita") {
    $inizio = $gioco->iniziaPartita();
    
    if (!$inizio) {
        echo "Impossibile iniziare la partita";
    }
    else {
        echo "Partita iniziata! Ora tocca a: ".$gioco->getColoreTurno();
    }
    exit();
}

if ($funzione=="getTurno") {
    $colore = $gioco->getColoreTurno();
    if (!$colore) {
        $stato = $gioco->getStatoPartita();
        switch ($stato){
            case 0:
                echo "Partita non iniziata";
                break;
            case 1:
                echo $colore."@Gioca ".$gioco->trovaNomeGiocatoredaColore($colore)." (".$colore.")";
                break;
            case 2:
                echo "Partita terminata";
                break;
        }
    }
    else {
        echo $colore."@Gioca ".$gioco->trovaNomeGiocatoredaColore($colore)." (".$colore.")";
    }
    exit;
}

if ($funzione=="getNomeGiocatore") {
    $coloreDaCercare = $_REQUEST['coloreDaCercare'];
    $ret = $gioco ->trovaNomeGiocatoredaColore($coloreDaCercare);
    echo $ret;	
    exit();
}

if ($funzione=="ottieniUltimeGiocate") {
    $ret = $gioco->ottieniUltimeGiocate(1);

    if (!$ret) {
        echo "Nessuna giocata";
    }
    else {
        for ($i=0;$i<count($ret);$i++){
            $colore = strtok($ret[$i], ":");
            echo "<DIV ID=log$i class=\"$colore log\">".$ret[$i]."</DIV>";
        }
    }
    exit();
}

if ($funzione=="gioca") {
    echo $gioco->gioca($mioColore,$_SERVER['REMOTE_ADDR']);
    exit();
}

scriviLog("masterServer", "Arrivata chiamata errata: codicePartita $codicePartita Errore nella chiamata: funzione $funzione non valida", "INFO");
echo "Errore nella chiamata: funzione $funzione non valida";
