<?php

include_once 'Partita.php';

echo "<PRE>";

$partita = new Partita();

echo "-> Inizia Partita ";
echo $partita->iniziaPartita()."\n";
echo "-> Stato Partita ";
echo $partita->debugStatoPartita()."\n";
echo "-> Aggiungo test1: ";
echo $partita->aggiungiGiocatore("test1","10.0.0.1")."\n";
echo "-> Inizia Partita: ";
echo $partita->iniziaPartita()."\n";
echo "-> Stato Partita: ";
echo $partita->debugStatoPartita()."\n";
echo "-> Aggiungo test2: ";
echo $partita->aggiungiGiocatore("test2","10.0.0.2")."\n";
echo "-> Aggiungo test3: ";
echo $partita->aggiungiGiocatore("test3","10.0.0.3")."\n";
echo "-> Stato Partita: ";
echo $partita->debugStatoPartita()."\n";
echo "-> Giocata verde: ";
echo $partita->giocata("verde","test1")."\n";
echo "-> Inizia Partita: ";
echo $partita->iniziaPartita()."\n";
echo "-> Giocata verde: ";
echo $partita->giocata("verde","10.0.0.1")."\n";
echo "-> Giocata verde: ";
echo $partita->giocata("verde","10.0.0.1")."\n";
echo "-> Giocata rosso: ";
echo $partita->giocata("rosso","10.0.0.2")."\n";
echo "-> Stato Partita: ";
echo $partita->debugStatoPartita()."\n";

echo "-> logGiocate";
$log = $partita->ottieniUltimeGiocate(5);
print_r( $log );

echo "</PRE>";
