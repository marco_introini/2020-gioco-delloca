<?php

session_start();

if (!isset(($_SESSION['codicePartita'])) || (!isset($_SESSION['mioColore']))) {
    header("Location:clientLogin.php");
}

include_once './Configurazione.php';

?>

<html>

<head>
    <style>
        .tondo {
            width: 100px;
            height: 100px;
            display: block;
            border: 1px solid black;
            border-radius: 50px;
            text-align: center;
            font-size: 40px;
            font-family: verdana;
            float: left;
        }
        
        .numero {
            position: relative;
            margin-top: 25px;
            display: block;
        }
        
        .verde {
            background-color: green;
        }
        
        .rosso {
            background-color: red;
        }
        
        .blu {
            background-color: blue;
        }
        
        .giallo {
            background-color: yellow;
        }
        
        .cerchietti span {
            border: 1px solid black;
            width: 16px;
            height: 16px;
            display: inline-block;
            border-radius: 8px;
        }
        
        #tavola {
            width: 70%;
            height: 100%;
        }
        
        .floatleft {
            float: left;
        }
    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

    <?php
    
    include_once './custom/custom.css';
    
    ?>
    
<link rel="apple-touch-icon" sizes="57x57" href="/custom/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/custom/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/custom/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/custom/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/custom/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/custom/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/custom/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/custom/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/custom/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/custom/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/custom/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/custom/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/custom/favicon/favicon-16x16.png">
<link rel="manifest" href="/custom/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    
</head>

<body>
    <div id="header">      
<?php

include_once 'custom/header.php';

?>   
    </div>
    
    <div id="attesaConnessione">
        Attesa di connessione al server di gioco...
    </div>    
    
    <div id="attesaPartita" style="display:none">
        
        <span id="messaggioAttesa">Partita 
        <?php
        echo $_SESSION['codicePartita'];
        ?><br>
        E' possibile iniziare la partita con almeno <?php echo $numeroMinimoGiocatori?> giocatori<br>
            In attesa di altri giocatori...</span>
        
        <span id="tuseiAttesa">Tu sei:
        <?php
        echo $_SESSION['mioColore'];
        ?>
        </span> 
        
        <span id="giocatoriAttesa">Giocatori presenti:</span><br>
        <span id="giocatore1" class="giocatore">Verde: Nessuno</span><br>
        <span id="giocatore2" class="giocatore">Rosso: Nessuno</span><br>
        <span id="giocatore3" class="giocatore">Blu: Nessuno</span><br>
        <span id="giocatore4" class="giocatore">Giallo: Nessuno</span><br>
        
        <button id ="iniziapartita" style="display:none">Inizia partita</button>
        
    </div>
    <div id=gameContainer>
        <div id=tavola class="floatleft" style="display:none">
            <script>
                for (i = 1; i < 41; i++) {
                    document.write("<div class=tondo>");
                    document.write("<div class=numero>" + i + "</div>");
                    document.write("<div style='clear:all'></div>");
                    document.write("<div class=cerchietti>");
                    document.write("<span class='verde verde" + i + "'> </span>");
                    document.write("<span class='rosso rosso" + i + "'> </span>");
                    document.write("<span class='blu blu" + i + "'> </span>");
                    document.write("<span class='giallo giallo" + i + "'> </span>");
                    document.write("</div>");
                    document.write("</div>");
                }
            </script>
        </div>
        <div id=pannello>
            <div id="informazioni" class="floatleft" style="display: none">

                    <span id="infopartita">
                PARTITA: 
                <?php
                echo $_SESSION['codicePartita'];
                ?>
                    </span>

                    <span id="infogiocatore">            
                IO SONO:
                <?php
                echo $_SESSION['mioColore'];
                ?>
                    </span>
                
                <br>
                
                <div id="stato" class="floatleft">
                    In attesa di connessione al server...
                </div>
                
                <div id="turno" class="floatleft">
                </div>
                
                <div id="ultimegiocate" class="floatleft" style='display:none'>
                </div>

                <div id="dado" class="floatleft" style='display:none'>
                </div>
                
                <div id="messaggio" class="floatleft" style='display:none'>
                </div>
                
            </div>

            <div id="pulsantiera" class="floatleft">    
                <button id ="verificaturno" style="display:none">Verifica turno</button>
                <button id ="iniziapartita" style="display:none">Inizia partita</button>
                <button id ="gioca" style="display:none">Lancia il dado</button>
                
                <form method=post action="clientLogout.php"><input id=logout type=submit value="ESCI"></form>

            </div>
        </div>
    </div>
    <div id="footer">
<?php

include_once 'custom/footer.php';

?>
    </div>
    
    <script>
        var posi = 0;

        // init
        $(".cerchietti").find("span").fadeOut();
        
        setInterval(function(){ 
            // metto qui le cose da eseguire ogni 5 secondi
            
            $('#attesaConnessione').hide();
            
            $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
            "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
            "funzione": "getStato"}
            }).done( function( result ) {
            
            split = result.split("@");
            
            statoPartita = split[0];
            
            if (statoPartita==0) {
                $('#informazioni').hide();
                $('#attesaPartita').show();
                // partita non iniziata
                $('#stato').text(split[1]);
                
                // recupero le info sui giocatori presenti
                descrizioneStato = split[1];
                numeroGiocatori = split[2];
                nomeVerde = split[3];
                
                $('#giocatoriAttesa').text("Numero giocatori registrati: " + numeroGiocatori);
                
                $('#giocatore1').text("Verde: " + nomeVerde);
                if (numeroGiocatori>=2) {
                    nomeRosso = split[4];
                    $('#giocatore2').text("Rosso: " + nomeRosso);
                }
                if (numeroGiocatori >= 3) {
                    nomeBlu = split[5];
                    $('#giocatore3').text("Blu: " + nomeBlu);
                }
                if (numeroGiocatori >= 4) {
                    nomeGiallo = split[6]; 
                    $('#giocatore4').text("Giallo: " + nomeGiallo);
                }
                
                if (numeroGiocatori>= <?php echo $numeroMinimoGiocatori?>) {
                    $('#messaggioAttesa').hide();
                    $('#iniziapartita').show();
                }
                
            }
            else if (statoPartita==1) {
                // partita in corso
                // recupero le info sui giocatori presenti
                $('#iniziapartita').hide();
                $('#attesaPartita').hide();
                $('#tavola').show();
                //$('#verificaturno').show();
		$('#gioca').text('Lancia il dado ');
                $('#gioca').hide();
                $('#informazioni').show();
                $('#ultimegiocate').show();
                
                descrizioneStato = split[1];
                numeroGiocatori = split[2];
                postoVerde = split[3];
                $(".cerchietti").find("span").fadeOut();
                $(".verde" + postoVerde).fadeIn();
                postoRosso = split[4];
                $(".rosso" + postoRosso).fadeIn();
                postoGiallo = 0;
                postoBlu = 0;
                if (numeroGiocatori == 3) {
                    postoBlu = split[5];
                    $(".blu" + postoBlu).fadeIn();
                    postoGiallo = 0;
                }
                if (numeroGiocatori == 4) {
                    postoBlu = split[5];
                    $(".blu" + postoBlu).fadeIn();
                    postoGiallo = split[6];
                    $(".giallo" + postoGiallo).fadeIn();   
                }
                
                $('#stato').text(split[1]);
                
                // ora recupero il turno
                $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
                "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
                "funzione": "getTurno"}
                }).done( function( result ) { 
                    split1 = result.split("@");
                    $('#turno').text(split1[1]);
                    if (split1[0]=='<?php echo $_SESSION['mioColore']?>'){
                        $('#gioca').show();
                        $("#messaggio").fadeOut("slow");
                    }
                });
                
                // ora recupero le ultime giocate
                $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
                "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
                "funzione": "ottieniUltimeGiocate"}
                }).done( function( result ) { 
                    $('#ultimegiocate').html(result);
                });
                
            }
            else if (statoPartita==2) {
                // partita conclusa
                $('#gioca').hide();
                $('#tavola').hide();
                $('#informazioni').show();
                $('#stato').text(split[1]);
                $('#ultimegiocate').show();
                
                // ora recupero le ultime giocate
                $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
                "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
                "funzione": "ottieniUltimeGiocate"}
                }).done( function( result ) { 
                    $('#ultimegiocate').html(result);
                });
                
            }
            else if ((statoPartita==3) || (statoPartita==4)) {
                // partita conclusa per timeout
                $('#gioca').hide();
                $('#tavola').hide();
                $('#informazioni').show();
                $('#stato').text(split[1]);
                $('#ultimegiocate').show();
                
                // ora recupero le ultime giocate
                $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
                "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
                "funzione": "ottieniUltimeGiocate"}
                }).done( function( result ) { 
                    $('#ultimegiocate').html(result);
                });
            }
            else {
                $('#stato').text('Errore reperimento stato partita: '+result); 
            }
            });
        }, 5000);

        $("#nored").click(function() {
            $(".rosso").fadeOut(20);
        });
        $("#sired").click(function() {
            $(".rosso").css("display", "inline-block");
        });
        $("#loopred").click(function() {
            posi++;
            if (posi > 41) posi = 1;
            $(".rosso").css("display", "none");
            $(".rosso" + posi).fadeIn(50);
        });
        $("#killall").click(function() {
            $(".cerchietti").find("span").fadeOut();
        });
        $("#showall").click(function() {
            $(".cerchietti").find("span").fadeIn();
        });
        
        $("#verificaturno").click(function() {
            $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
            "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
            "funzione": "getTurno"}
            }).done( function( result ) { alert( result ); });
                
        });
        $("#statopartita").click(function() {
            $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
            "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
            "funzione": "getStato"}
            }).done( function( result ) { alert( result ); });
                
        });
        $("#gioca").click(function() {
            $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
            "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
            "funzione": "gioca"}
            }).done( function( result ) { 
            
                    /*
         * CODICI RITORNO
         * 
         * Errore:
         * 0@Descizione
         * 
         * Giocata normale
         * Se non ci sono imprevisti
         * 1@giocata@valore lancio dado@posizione finale
         * Se ci sono imprevisti
         * 2@giocata@valore lancio dado@descrizione imprevisto@descrizione comando@posizione finale
         * 
         * Salto turno
         * 3@Descrizione
         * 
         * Vittoria:
         * 4@Messaggio
         * 
         */
 
            $('#messaggio').show();
            split = result.split("@");
            
            codice = split[0];
            messaggioVocale ="";
            
            if (codice == 0) {
                $('#messaggio').text(split[1]);
                messaggioVocale = split[1];
            }
            else if (codice == 1) {
                $('#messaggio').text("Hai fatto "+split[2]+"\n\Sei arrivato alla posizione "+split[3]);
                messaggioVocale = "Hai fatto "+split[2]+". Sei arrivato alla posizione "+split[3];
                dado(split[2]);
            }
            else if (codice == 2) {
                dado(split[2]);
                $('#messaggio').text("Hai fatto "+split[2]+".\n\
"+split[3]+"\n\
"+split[4]+"\n\
Sei arrivato alla posizione "+split[5]);
                messaggioVocale = "Hai fatto "+split[2]+".\
"+split[3]+"\
"+split[4]+"\
Sei arrivato alla posizione "+split[5];
            }
            else if (codice == 3) {
                $('#messaggio').text(split[1]);
                messaggioVocale = split[1];
            }
            else if (codice == 4) {
                $('#messaggio').text(split[1]);
                messaggioVocale = split[1];
            }
             var msg = new SpeechSynthesisUtterance(messaggioVocale);
             window.speechSynthesis.speak(msg);
           
            $('#gioca').hide();
            setTimeout(function(){
                $('#dado').fadeOut();
            },3000);
             
        });
                
        });
        $("#iniziapartita").click(function() {
            $.ajax({ 
                url: "masterServer.php",
                type: 'get',
                data: { "codicePartita": "<?php echo $_SESSION['codicePartita']; ?>",
            "mioColore": "<?php echo $_SESSION['mioColore']; ?>",
            "funzione": "iniziaPartita"}
            }).done( function( result ) { 
            // PARTITA INIZIATA
             });
                
        });


    function dado(point) {
            if (point<0) {
                $("#dado").fadeOut();
            } else {
                $("#dado").text(point).fadeIn();
   
                $("#dado").text(point).fadeIn();
            }
        }

        
    </script>

</body>

</html>