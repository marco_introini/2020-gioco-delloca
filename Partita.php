<?php

include_once 'Configurazione.php';
include_once 'Giocatore.php';
include_once 'Azione.php';

/**
 * Classe della partita del gioco dell'oca
 *
 * @author Marco Introini
 * 
 * giocatori: verde rosso blu giallo
 */
class Partita {
    private $codicePartita;
    private $statoPartita; // 0=non iniziata, 1=iniziata, 2=conclusa, 3 = abortita per timeout, 4 = abortita per timeout sul turno
    private $logPartita;
    private $turno;
    private $giocatori;
    private $timestampInizio = null;
    private $timeStampTurno = null;

    /**
     * Partita constructor.
     */
    public function __construct()
    {
        $this->codicePartita = millisecondi();
        $this->turno = 1;
        $this->giocatori = [
            1 => null,
            2 => null,
            3 => null,
            4 => null
        ];
        $this->statoPartita = 0; 
        $this->timestampInizio = microtime(true);
        $this->logPartita[0] = "";
    }
    
    public function postoDisponibile() {
        
        for ($i=1; $i<=4; $i++) {
            if ($this->giocatori[$i] == null) {
                scriviLog(__METHOD__, "Partita ".$this->codicePartita." Posto disponibile: $i", "DEBUG");
                return $i;
            }
        }
        scriviLog(__METHOD__, "Partita ".$this->codicePartita." Nessun posto disponibile", "DEBUG");
        return false; // nessun posto disponibile
    }

    public function aggiungiGiocatore($nome, $ip) {
        $disponibile = $this->postoDisponibile();
        if (!$disponibile) {
            scriviLog(__METHOD__, "Partita ".$this->codicePartita." Nessun posto disponibile", "DEBUG");
            return false;
        } // partita piena

        //individuo il colore
        $colore = $this->trovaColoreGiocatore($disponibile);

        $this->giocatori[$disponibile] = new Giocatore($nome, $colore, $ip);
        scriviLog(__METHOD__, "Partita ".$this->codicePartita." Aggiunta giocatore: $colore", "DEBUG");
        return $disponibile."@".$colore;
    }
    
    public function iniziaPartita() {
        global $numeroMinimoGiocatori;
        
        if ($this->statoPartita!=0) {
            return false;
        } //partita già iniziata o conclusa
        
        if ($this->numeroGiocatori() >= $numeroMinimoGiocatori) {
            $this->turno = 1;
            $this->statoPartita=1;
            $this->timestampInizio = microtime(true);
            $this->logPartita[0] = "Inizio partita";
            scriviLog(__METHOD__, "Partita ".$this->codicePartita."Inizio partita", "DEBUG");
            return true; // partita iniziata
        }
        return false;
    }
    
    private function verificaPartitaValida() {
        global $tempoTimeoutPartita, $tempoTimeoutTurno;
        
        // il timeout vale solo se la partita è in corso o ancora non iniziata
        
        if ($this->statoPartita > 1) {
            return true;
        }
        
        $tempoAttuale = microtime(true);
        
        if ($tempoAttuale - $this->timestampInizio > $tempoTimeoutPartita) {
            // partita abortita per timeout
            $this->statoPartita = 3;
            return false;
        }
        
        if (($this->timeStampTurno != null) && ($tempoAttuale - $this->timeStampTurno > $tempoTimeoutTurno)) {
            // partita abortita per timeout di un giocatore
            $this->statoPartita = 4;
            return false;
        }
        
        return true;
        
    }
    
    public function getTurno() {
        if (!$this->verificaPartitaValida()) {
            return false;
        }
        return $this->turno;
    }
    
    public function getColoreTurno() {
        // non devo passare da getTurno perchè deve andare anche in caso di timeout
        return $this->trovaColoreGiocatore($this->turno);
    }
    
    private function trovaNumeroGiocatore($colore) {
        $numero = 0;
        switch ($colore) {
            case "verde":
                $numero = 1;
                break;
            case "rosso":
                $numero = 2;
                break;
            case "blu":
                $numero = 3;
                break;
            case "giallo":
                $numero = 4;
                break;
            default:
                return false;
        }
        return $numero;
    } 
    
    private function trovaColoreGiocatore($numero) {
        $colore = null;
        switch ($numero) {
            case 1:
                $colore = "verde";
                break;
            case 2:
                $colore = "rosso";
                break;
            case 3:
                $colore = "blu";
                break;
            case 4:
                $colore = "giallo";
                break;
            default:
                return false;
        }
        return $colore;
    } 
    
    public function trovaNomeGiocatoredaColore($colore) {

        for ($i=1; $i <= $this->numeroGiocatori(); $i++) {
            if ($this->giocatori[$i]->getColore() == $colore) {
                return $this->giocatori[$i]->getNome();
            }
              
        }
        
        return false;
    }

    public function mioTurno($giocatore, $ip) {
        
        $ret = [
            "stato" => false,
            "messaggio" => ""
        ];
        
        if ($this->statoPartita == 0) {
            $ret["stato"] = false;
            $ret["messaggio"] = "Partita non iniziata";
            return $ret;
        }
        if ($this->statoPartita == 2) {
            $ret["stato"] = false;
            $ret["messaggio"] = "Partita conclusa";
            return $ret;
        }
        if ($this->statoPartita == 3) {
            $ret["stato"] = false;
            $ret["messaggio"] = "Partita conclusa a causa di un timeout globale";
            return $ret;
        }
        if ($this->statoPartita == 4) {
            $ret["stato"] = false;
            $ret["messaggio"] = "Partita conclusa a causa di un timeout di ".$this->getColoreTurno();
            return $ret;
        }
        $numeroGiocatore = $this->trovaNumeroGiocatore($giocatore);
        
        if (!$numeroGiocatore) {
            $ret["stato"] = false;
            $ret["messaggio"] = "Giocatore errato";
            return $ret;
        }
        
        if ($this->getTurno() != $numeroGiocatore) {
            $ret["stato"] = false;
            $ret["messaggio"] = "Turno di " . $this->trovaColoreGiocatore($this->getTurno());
            return $ret;
        }

        // controlla IP
        if (!$this->giocatori[$numeroGiocatore]->controllaIp($ip)) {
            $ret["stato"] = false;
            $ret["messaggio"] = "IP errato";
            return $ret;
        }
              
        $ret["stato"] = true;
        $ret["messaggio"] = "Mio Turno";
        return $ret;
    }


    public function giocata($giocatore, $ip) {
        /*
         * CODICI RITORNO
         * 
         * Errore:
         * 0@Descizione
         * 
         * Giocata normale
         * Se non ci sono imprevisti
         * 1@giocata@valore lancio dado@posizione finale
         * Se ci sono imprevisti
         * 2@giocata@valore lancio dado@descrizione imprevisto@descrizione comando@posizione finale
         * 
         * Salto turno
         * 3@Descrizione
         * 
         * Vittoria:
         * 4@Messaggio
         * 
         */
        global $numeroCaselle;
        
        if ($this->getStato() != 1) {
            return "0@".$this->getStatoLeggibile();
        }
        
        $controlloTurno = $this->mioTurno($giocatore, $ip);
        if (!$controlloTurno["stato"]) {
            return "0@".$controlloTurno["messaggio"];
        }
        
        $numeroGiocatore = $this->trovaNumeroGiocatore($giocatore);
        
        // come prima cosa verifico non debba saltare il turno
        if ($this->giocatori[$numeroGiocatore]->getSaltaTurno()== true) {
            $this->giocatori[$numeroGiocatore]->setSaltaTurno(false);
            $this->cambiaTurno();
            array_push($this->logPartita,"$giocatore: ha saltato il turno");
            return "3@Hai saltato il turno! Aspetta il prossimo giro";
        }

        // imposto il tempo per il timeout turno successivo
        $this->timeStampTurno = microtime(true);
        
        // lancio il dado
        $dado = rand(0,5);
        $this->giocatori[$numeroGiocatore]->avanza($dado);
        if ($this->giocatori[$numeroGiocatore]->getPosizione() === $numeroCaselle) {
            $this->statoPartita = 2;
            array_push($this->logPartita,"$giocatore: ha fatto $dado. Arrivato alla posizione <span class=posizione>"
                .$this->giocatori[$numeroGiocatore]->getPosizione()."</span> e ha VINTO!");
            return "4@Complimenti, hai vinto!";
        }
        
        // genero l'azione
        $azione = new Azione();
        $azioneAttuale = $azione->getAzione();
        
        // dopo la giocata sistemo i turni
        $this->cambiaTurno();

        // qui non ci sono comandi particolari. Torno le info della giocata e basta
        if (($dado == 0) || ($azioneAttuale["codiceComando"]=="na")) {
            array_push($this->logPartita,strval("$giocatore: ha fatto ".$dado.". Arrivato alla posizione <span class=posizione>".$this->giocatori[$numeroGiocatore]->getPosizione()."</span>"));
            return "1@giocata@".$dado."@".$this->giocatori[$numeroGiocatore]->getPosizione();
        }
        
        // controllo il comando
        /*
         *  "+1" => "Avanti di una casella",
            "-1" => "Indietro di una casella",
            "+2" => "Avanti di due caselle",
            "-2" => "Indietro di due caselle",
            "+3" => "Avanti di tre caselle",
            "-3" => "Indietro di tre caselle",
            "f1" => "Fermo un turno",
            "ta" => "Tira Ancora",
            "xx" => "Torna all'inizio",
            "na" => "Nessuno"
         */
        
        switch ($azioneAttuale["codiceComando"]) {
            case "+1":
                $this->giocatori[$numeroGiocatore]->setPosizione($this->giocatori[$numeroGiocatore]->getPosizione()+1);
                break;
            case "-1":
                $nuovaPosizione = $this->giocatori[$numeroGiocatore]->getPosizione()-1;
                if ($nuovaPosizione < 0) {
                    $nuovaPosizione = 0;
                }
                $this->giocatori[$numeroGiocatore]->setPosizione($nuovaPosizione);
                break;
            case "+2":
                $this->giocatori[$numeroGiocatore]->setPosizione($this->giocatori[$numeroGiocatore]->getPosizione()+2);
                break;
            case "-2":
                $nuovaPosizione = $this->giocatori[$numeroGiocatore]->getPosizione()-2;
                if ($nuovaPosizione < 0) {
                    $nuovaPosizione = 0;
                }
                $this->giocatori[$numeroGiocatore]->setPosizione($nuovaPosizione);
                break;
            case "+3":
                $this->giocatori[$numeroGiocatore]->setPosizione($this->giocatori[$numeroGiocatore]->getPosizione()+3);
                break;
            case "-3":
                $nuovaPosizione = $this->giocatori[$numeroGiocatore]->getPosizione()-3;
                if ($nuovaPosizione < 0) {
                    $nuovaPosizione = 0;
                }
                $this->giocatori[$numeroGiocatore]->setPosizione($nuovaPosizione);
                break;
            case "f1":
                $this->giocatori[$numeroGiocatore]->setSaltaTurno(true);
                break;
            case "ta":
                $this->turno = $numeroGiocatore;
                break;
            case "xx":
                $this->giocatori[$numeroGiocatore]->setPosizione(0);
                break;
        }
        
        // dopo aver spostato le caselle secondo l'imprevisto uscito verifico nuovamente
        // se non abbia vinto
        
        if ($this->giocatori[$numeroGiocatore]->getPosizione() == $numeroCaselle) {
            $this->statoPartita = 2;
        array_push($this->logPartita,strval(
                $giocatore.": ha fatto ".$dado." ma ha trovato un imprevisto: "
                .$azioneAttuale["azione"]." "
                ."<span class=tiraancora>".$azioneAttuale["descrizioneComando"]."</span>"
                ."Arrivato alla posizione "
                ."<span class=posizione>".$this->giocatori[$numeroGiocatore]->getPosizione()."</span> e HA VINTO!"
                ));
            return "4@Complimenti, hai vinto!";
        }
        
        array_push($this->logPartita,strval(
                $giocatore.": ha fatto ".$dado." ma ha trovato un imprevisto: "
                .$azioneAttuale["azione"]." "
                ."<span class=tiraancora>".$azioneAttuale["descrizioneComando"]."</span>"
                ."Arrivato alla posizione "
                ."<span class=posizione>".$this->giocatori[$numeroGiocatore]->getPosizione()."</span>"
                ));
        
        return "2@giocata con imprevisto@".$dado."@".$azioneAttuale["azione"]."@".$azioneAttuale["descrizioneComando"]."@".$this->giocatori[$numeroGiocatore]->getPosizione();
        
    }

    private function cambiaTurno() {
        $this->turno++;
        if ($this->turno>$this->numeroGiocatori()) {
            $this->turno = 1;
        }
    }
    
    private function numeroGiocatori() {
        for ($i=1; $i<=4; $i++) {
            if ($this->giocatori[$i] == null) {
                $ret = $i-1;
                return $ret;
            }
        }
        return 4;
    }
    
    public function debugStatoPartita() {
        $ret="STATO PARTITA ". $this->codicePartita."\n";

        $ret.="Stato: ".$this->statoPartita."\n";
        $ret.="Stato leggibile: ".$this->getStatoLeggibile()."\n";
        $ret.="Timestamp Inizio Partita: ".$this->timestampInizio."\n";
        $ret.="Timestamp Turno: ".$this->timeStampTurno."\n";
        
        for ($i=1; $i<=4; $i++) {
            $ret.="Giocatore$i: ";
            if ($this->giocatori[$i] == null) {
                $ret.="Vuoto";
            }
            else {
                $ret.=$this->giocatori[$i]->getColore();
                $ret.=" posizione: ".$this->giocatori[$i]->getPosizione();
            }
            
            $ret.="\n";
        }
        
        $ret.="Turno: ". $this->getColoreTurno()."\n";
        
        $ret.="Ultime 5 giocate: ". $this->ottieniUltimeGiocate(5)."\n";
        
        $ret.="\nFINE STATO PARTITA\n";
        
        return $ret;
    }

    public function ottieniUltimeGiocate($numero) {
        // torna le ultime n giocate prendendole dal log
        $ret = []; 
        if ($numero >= count($this->logPartita)) {
            $ret = $this->logPartita; // copia dell'array (default php)
        }
        else {
            $numeroDa = count($this->logPartita) - $numero;
            for ($i=$numeroDa;$i<=count($this->logPartita);$i++) {
                array_push($ret, $this->logPartita[$i]);
            }
        }
        
        return $ret;
    }
    
    public function getCodice() {
        return $this->codicePartita;
    }
    
    public function getPosizioniGiocatori() {
        $ret = "";
        
        if ($this->getStato() != 1) {
            // stato non coerente con le posizioni
            return false;
        }
        $ret .= $this->numeroGiocatori();

        for ($i=1; $i <= $this->numeroGiocatori(); $i++) {
            $ret.="@";
            $ret.=$this->giocatori[$i]->getPosizione();
        }
        
        return $ret;
    }
    
    public function getNomiGiocatori() {
        $ret = "";
        $ret .= $this->numeroGiocatori();

        for ($i=1; $i <= $this->numeroGiocatori(); $i++) {
            $ret.="@";
            $ret.=$this->giocatori[$i]->getNome();
        }
        
        return $ret;
    }
    
    public function getStato() {
        $this->verificaPartitaValida();
        return $this->statoPartita;
    }
    
    public function getStatoLeggibile() {
        
        $mess = "";
        
        switch ($this->getStato()) {
            case 0:
                $mess = "Partita non iniziata";
                break;
            case 1:
                $mess = "Partita in corso";
                break;
            case 2:
                $vincitore = $this->trovaVincitore();
                if (!$vincitore) {
                    $mess = "Errore nel recupero del nome del vincitore";
                    break;
                }
                $mess = "Partita terminata. Vincitore: ". $this->trovaColoreGiocatore($vincitore)
                   ." Nome: ".$this->giocatori[$vincitore]->getNome();
                break;
            case 3:
                $mess = "Partita terminata a causa di un timeout";
                break;
            case 4:
                $mess = "Partita terminata a causa di un timeout sul turno di ".$this->getColoreTurno();
                break;
            default :
                $mess = "Errore nel reperimento informazioni sullo stato partita";
        }
        
        return $mess;
    }
    
    public function verificaMiaPartita($colore,$nome) {
        /*
         * Verifico che il giocatore sia effettivamente dentro la partita
         */
        
        $numero = $this->trovaNumeroGiocatore($colore);
        
        if ($this->giocatori[$numero] == null) {
            return false;
        }
        return $this->giocatori[$numero]->verificaNome($nome);
        
    }
    
    public function trovaVincitore() {
        if ($this->statoPartita != 2) {
            // partita non conclusa
            return false;
        }
       
        for ($i=1; $i<= $this->numeroGiocatori(); $i++) {
            if ($this->giocatori[$i]->getPosizione() >= 40) {
                return $i;
            }
        }
        
        return false;
    }
}
