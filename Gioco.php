<?php

include_once 'Configurazione.php';
include_once 'SchedaGioco.php';
include_once 'Partita.php';

/**
 * Gioco si preoccupa della gestione su db della partita e degli aspetti relativi
 * alla concorrenza
 *
 * @author Marco Introini
 */
class Gioco {
    /** @var Partita */
    private $partita;
    /** @var PDO */
    private $connessioneDb;
    
    public function __construct(){
        $this->partite = null;
        
        $this->inizializza();
    }
    
    public function inizializza() {
        global $db_host,$db_password,$db_user,$db_name;
        
        try {
            $this->connessioneDb = new PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_user, $db_password);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        
    }
    
    public function caricaPartita($codicePartita) {
        
        $sql = "SELECT oggetto,stato FROM partite"
                . " WHERE codice = :codicePartita";

        $stmt = $this->connessioneDb->prepare($sql);
        $stmt->bindValue(':codicePartita', $codicePartita, PDO::PARAM_INT);
        
        $stato = null;
        $oggetto = null;
        
        $stmt->execute();    
        $stmt->bindColumn(2, $stato);

        $stmt->bindColumn(1, $oggetto, PDO::PARAM_LOB);

        if ($stmt->fetch(PDO::FETCH_BOUND)) {
            // occorrenza trovata           
            $this->partita = unserialize($oggetto);
            return true;
        }
        else {
            // occorrenza non trovata
            return false;
        }
   
    }
    
    public function getCodicePartita() {
        if ($this->partita == null) {
            return null;
        }
        
        return $this->partita->getCodice();
    }
    
    public function ottieniUltimeGiocate($numero) {
        if ($this->partita == null) {
            return false;
        }
        
        return $this->partita->ottieniUltimeGiocate($numero);
    }
    
    public function verificaMiaPartita($codicePartita, $colore, $nome) {
        if ($this->partita == null) {
            $this->caricaPartita($codicePartita);
        }
        
        if ($this->partita->getCodice() != $codicePartita) {
            return false;
        }
        
        if ($this->partita->verificaMiaPartita($colore, $nome)) {
            return true;
        }
        
        return false;
    }
    
    public function trovaPartitaDisponibile() {
        /*
         * Trovo una partita disponibile
         */
        
        $sql = "SELECT codice,oggetto FROM partite"
                . " WHERE stato = :statoPartita";

        $stmt = $this->connessioneDb->prepare($sql);
        // prendo solo le partite disponibili non iniziate o finite
        $stmt->bindValue(':statoPartita', 0, PDO::PARAM_INT);
        
        $codice = null;
        $oggetto = null;
        
        $stmt->execute();    
        $stmt->bindColumn(1, $codice);

        $stmt->bindColumn(2, $oggetto, PDO::PARAM_LOB);
        //$stmt->debugDumpParams();

        while($stmt->fetch(PDO::FETCH_BOUND)) {
            // occorrenza trovata: torno il codice partita
            $this->partita = unserialize($oggetto);
            if ($this->partita->getStato() != 0) {
                // questo caso serve per capire se la partita è andata in timeout
                // in questo caso salvo la partita in modo da aggiornare lo stato su db
                $this->salvaPartita();
                continue;
            }
            
            // verifico che ci sia posto nella partita
            if (!$this->partita->postoDisponibile()) {
                continue;
            }
            
            return $this->partita->getCodice();
        }
        
        // nessun record trovato
        return false;
        
    }
    
    public function nuovaPartita() {
        /*if ($this->partita != null) {
            // impossibile creare nuova partita: già in corso
            return false;
        }*/
        
        $this->partita = new Partita();
        
        $sql = "INSERT INTO partite (codice,oggetto,stato)
                VALUES (:codicePartita,:oggetto,0);";

        $stmt = $this->connessioneDb->prepare($sql);

        $stmt->bindParam(':oggetto', serialize($this->partita), PDO::PARAM_LOB);
        $stmt->bindParam(':codicePartita', $this->partita->getCodice());

        $ret = $stmt->execute();
        
        if ($ret) {
            // inserimento andato bene: torno il codice partita
            return $this->partita->getCodice();
        }
        else {
            return FALSE;
        }
        
    }
    
    public function salvaPartita() {
        if ($this->partita == null) {
            // impossibile salvare la partita: ancora non creata
            return false;
        }
        
        $sql = "UPDATE partite 
                SET
                 oggetto = :oggetto,
                 stato = :stato
                WHERE codice = :codicePartita";

        $stmt = $this->connessioneDb->prepare($sql);

        $stmt->bindParam(':oggetto', serialize($this->partita), PDO::PARAM_LOB);
        $stmt->bindParam(':codicePartita', $this->partita->getCodice());
        $stmt->bindParam(':stato', $this->partita->getStato());

        $ret = $stmt->execute();
        
        return $ret;
        
    }
    
    public function aggiungiGiocatore($nome, $ip) {
        if ($this->partita != null ){

            $ret = $this->partita->aggiungiGiocatore($nome, $ip);
            if ($ret) {
                // salvo solo se c'è stato un cambiamento
                $this->salvaPartita();
            }
            else {
                scriviLog(__METHOD__, "AggiungiGiocatore fallito per metodo su Partita", "INFO");   
            }
            return $ret;
        }
        else {
            // nessuna partita
            scriviLog(__METHOD__, "Trovata partita NULL", "INFO");
            return false;
        }
        
    }
    
    public function gioca($giocatore,$ip) {
        if ($this->partita != null ){
            
            $ret = $this->partita->giocata($giocatore, $ip);
            
            if ($ret) {
                // salvo solo se c'è stato un cambiamento
                $this->salvaPartita();
            }
            scriviLog(__METHOD__, "Partita ".$this->getCodicePartita()." Giocata di $giocatore: $ret", "DEBUG");
            
            return $ret;
        }
        else {
            // nessuna partita
            return false;
        }
    }

    public function getStringaDebug() {
        if ($this->partita!=null) {
            return $this->partita->debugStatoPartita();
        }
        return false;
    }
    
    public function getPosizioniGiocatori() {
        if ($this->partita!=null) {           
            return $this->partita->getPosizioniGiocatori();
        }
        return false;        
    }
    
     public function getNomiGiocatori() {
        if ($this->partita!=null) {           
            return $this->partita->getNomiGiocatori();
        }
        return false;        
    }
    
    public function getStatoPartitaLeggibile() {        
        if ($this->partita!=null) {
            $ret = $this->partita->getStatoLeggibile();
            return $ret;
        }
        return false;
    }
    
    public function getStatoPartita() {
        if ($this->partita!=null) {
            return $this->partita->getStato();
        }
        return false;
    }

    public function getColoreTurno() {
        if ($this->partita!=null) {
            return $this->partita->getColoreTurno();
        }
        return false;    
    }
    
    public function iniziaPartita() {
        if ($this->partita!=null) {
            $retIniziaPartita = $this->partita->iniziaPartita();
            if ($retIniziaPartita) {
                $this->salvaPartita();
            }
            return $retIniziaPartita;
        }
        return false;  
    }
    
    public function trovaNomeGiocatoredaColore($colore) {
        if ($this->partita!=null) {
            return $this->partita->trovaNomeGiocatoredaColore($colore);
        }
        return false;
    }
    
    public function __destruct() {
        
        $this->salvaGioco();
        $this->connessioneDb = null;
    }
    
}
